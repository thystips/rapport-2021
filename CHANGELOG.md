# CHANGELOG

<!--- next entry here -->

## 0.7.3
2021-08-18

### Fixes

- **doc:** [WIP] Julien's reco v3 (cd72708623e11abf32df785cf59de36d9aa6a6d9)

## 0.7.1
2021-08-11

### Fixes

- **doc:** Julien's reco (2dd2ac5293e21bebc50006478dc33e82d8b3fc54)
- **repo:** clean folder (3790179460bb2930784e4aa1c2f51a39441277b3)
- **images:** Fix image format (6c9527d6e0c85351f4279ff111e244a7db1af667)

## 0.7.0
2021-07-30

### Features

- **doc:** Add remerciements's chapter (0354ea4b5c2d119eccaf3c2f2545e09b80f7aaca)

### Fixes

- **doc:** Spellchecking (1cb1451a65f7dc8e8f8268f98407bcd156137352)
- **doc:** Footnote to Bibliography (a98f63ce011ae09e5f579fc9abb016d02ea1bf51)
- **marche:** Fix acronym (d6b67cf333b70f6e1cd2d785d047045e4d1bf082)
- **link:** Fix links style (6e079bb37b63a3cf154e03fd23b9ac66db96c6a9)
- **doc:** Fix some stuff (722a3dd571a1a9bda9026ad16838a3748bc7dcb6)

## 0.6.1
2021-07-29

### Fixes

- **images:** png to eps (2289d1ca4f16afd1f57fef80f6834f5736e0abd2)
- **doc:** Define Header and Footer (653bfb5b119c3c0c63665e46e912aa7dffe4dce0)

## 0.6.0
2021-07-29

### Features

- **conclusion:** Add conclusion (a02e20c18f4a4a296e69a338aa7690eafecfe0be)

## 0.5.0
2021-07-29

### Features

- **bilan:** Add Bilan's section (e9c82330ff4aaaae76539e68dc3e1f9a75a975fd)

### Fixes

- **doc:** Fix some stuff (7d68e800b5825ace187b748103c6b2efa23d2fea)

## 0.4.0
2021-07-28

### Features

- **mission:** Add NovNet project (bfdafe0e150a1e7b4ccab77152f65466fe0f2547)

### Fixes

- **grosse maj:** Ajout de sous sections (27ff6c989e41f297072343dc7ec13830eca13512)
- **ci:** use latexmk instead pdflatex (301f1f05d00c93c70fb7a926441f3244ac69b2e9)
- **images:** Use eps instead png (5dc15ed2a7af63c8ecebd4dccfd08fc127faf1cd)
- **doc:** Change mission to Project (16d8de14e43dd9e5aacfb96f3efd4ece4bffcc0d)
- **mission:** End projet grosse maj (998db5a0d6451aae0b791523fc42d3175b78985f)
- **doc:** Page number (6d0548491375ca368dc0dbc356beebc805eef015)

## 0.3.1
2021-07-27

### Fixes

- **grosse maj:** Ajout de sous sections (3e0f08ea790cc1279c54643689f739e454b05ed6)

## 0.3.0
2021-07-26

### Features

- **rapport:** WIP - Missions (19e66f9cdf5aadec07d63c2a186ffe9ddb563f53)

## 0.2.0
2021-07-23

### Features

- **entreprise:** Marchés (0f7021d6bdf920e56af2bc0d4a7138c3c0a278ab)

## 0.1.0
2021-07-22

### Features

- **ci:** Auto release (067eb6e40379435be7401028a5f491712df9ba42)

### Fixes

- **ci:** Auto release (efbf3a4760808391db8f7f41aef939ba5e8a0f82)
- **ci:** Auto release +x (2ca1605fabd24a08d1629733d6ebe760af71bd7c)